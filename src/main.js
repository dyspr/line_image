var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var slit
var frame = 0
var direction = 0

function preload() {
  slit = loadImage('img/fortepan_191049.jpg')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  image(slit.get(Math.floor((slit.width * 0.5) + frame * (slit.width * 0.5)), 0, 1, slit.height), windowWidth * 0.5 - boardSize * 0.45, windowHeight * 0.5 - boardSize * 0.45, boardSize * 0.9, boardSize * 0.9)

  if (direction === 0) {
    frame += deltaTime * 0.00005
    if (frame >= 0.999) {
      direction = 1
    }
  } else {
    frame -= deltaTime * 0.00005
    if (frame <= 0.001) {
      direction = 0
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
